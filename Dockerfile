FROM python:3
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
# Install Pillow
RUN apt-get update && apt-get install -y \
    libjpeg-dev \
    zlib1g-dev
COPY . /code/

# code directory of the container

# FROM python:3
# ENV PYTHONDONTWRITEBYTECODE=1
# ENV PYTHONUNBUFFERED=1
# WORKDIR /code

# # Install Node.js and NPM
# RUN apt-get update && apt-get install -y curl
# RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
# RUN apt-get install -y nodejs

# # Copy and install Python dependencies
# COPY requirements.txt /code/
# RUN pip install -r requirements.txt

# # Install Node dependencies and build React app
# FROM node:13.12.0-alpine
# COPY package.json ./
# COPY package-lock.json ./
# RUN npm install --silent
# RUN npm install react-scripts@3.4.1 -g --silent


# #Install Pillow
# RUN apt-get update && apt-get install -y \
#     libjpeg-dev \
#     zlib1g-dev
# # Add your remaining Dockerfile steps
# # ...

# # Copy the rest of the code
# COPY . /code/
