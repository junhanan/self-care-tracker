from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import UserVO, Activity

# Create your views here.

class UserVOEncoder(ModelEncoder):
    model = UserVO
    properties = [
        "username",
        "id"
    ]


class ActivityEncoder(ModelEncoder):
    model = Activity
    properties = [
        "activity_name",
        "description",
        "created_at",
        "updated_at"
    ]

@require_http_methods(["GET", "POST"])
def api_activity(request, id=None):
    if request.method == "GET":
        return JsonResponse(
            {"acitivity": Activity.objects.all()},
            encoder=ActivityEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            all_activity = Activity.objects.create(**content)
            return JsonResponse(
                all_activity,
                encoder=ActivityEncoder,
                safe=False,
            )
        except TypeError as e:
            return JsonResponse({"message": "Type error: " + str(e)})
