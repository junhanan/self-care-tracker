from django.db import models

# Create your models here.
class UserVO(models.Model):
    username = models.CharField(max_length=100)



class Activity(models.Model):
    activity_name = models.CharField(max_length=255)
    description = models.TextField()
    # image = models.ImageField(upload_to='activity_images', blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Mood(models.Model):
    MOOD_CHOICES = (
        ('happy', 'Happy'),
        ('sad', 'Sad'),
        ('neutral', 'Neutral'),
        # Add other mood choices as needed
    )

    user = models.ForeignKey(UserVO, on_delete=models.CASCADE)
    mood_level = models.CharField(max_length=255, choices=MOOD_CHOICES)
    note = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.user.username} - {self.mood_level}"


class EnergyLevel(models.Model):
    ENERGY_CHOICES = (
        ('low', 'Low'),
        ('medium', 'Medium'),
        ('high', 'High'),
        # Add other energy level choices as needed
    )

    user = models.ForeignKey(UserVO, on_delete=models.CASCADE)
    energy_level = models.CharField(max_length=255, choices=ENERGY_CHOICES)
    note = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.user.username} - {self.energy_level}"


class Sleep(models.Model):
    user = models.ForeignKey(UserVO, on_delete=models.CASCADE)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    note = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.user.username} - {self.start_time} to {self.end_time}"
