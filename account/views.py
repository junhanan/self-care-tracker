from django.shortcuts import render
from .models import UserProfile
from .common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
# Create your views here.


class UserProfileEncoder(ModelEncoder):
    model = UserProfile
    properties = [
        "user",
        "bio",
        "profile_image"
    ]

@require_http_methods(["GET", "POST"])
def api_user_profile(request, id=None):
    if request.method == "GET":
        return JsonResponse(
            {"user_profile": UserProfile.objects.all()},
            encoder=UserProfileEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            user_profile = UserProfile.objects.create(**content)
            return JsonResponse(
                user_profile,
                encoder=UserProfileEncoder,
                safe=False,
            )
        except TypeError as e:
            return JsonResponse(
                {"message": "Type error occurred: " + str(e)},
                status=400,
            )
