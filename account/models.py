from django.db import models
from tracker.models import UserVO

class UserProfile(models.Model):
    user = models.OneToOneField(UserVO, on_delete=models.CASCADE)
    bio = models.TextField(blank=True)
    profile_image = models.ImageField(upload_to='profile_images', blank=True, null=True)

    def __str__(self):
        return self.user.username

class UserSettings(models.Model):
    user = models.OneToOneField(UserVO, on_delete=models.CASCADE)
    theme = models.CharField(max_length=255)
    notification_enabled = models.BooleanField(default=True)

    def __str__(self):
        return self.user.username

# Add more user-related models as needed
